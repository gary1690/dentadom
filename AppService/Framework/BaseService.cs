﻿using Data.Framework;
namespace AppService.Framework
{
    public class BaseService : IBaseService
    {
        protected IUnitOfWork UnitOfWork;

        public BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public void Dispose()
        {
            UnitOfWork?.Dispose();
        }
    }
}
