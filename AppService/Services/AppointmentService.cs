﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Globalization;
using System.Data.Entity;

namespace AppService.Services
{
    public class AppointmentService : BaseService, IAppointmentService
    {
        public AppointmentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        
        //registers new appointment 
        public async Task<ResponseModel> CreateAppointment(string startDate, string endDate, int patientId, int userId)
        {
            var response = new ResponseModel();
            Appointment appointment = new Appointment()
            {
                StartDate = DateTime.ParseExact(startDate, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                EndDate = DateTime.ParseExact(endDate, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                UserId =userId,
                PatientId = patientId

            };
            try
            {
                UnitOfWork.Add(appointment);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.SetResult(appointment);
            }
            catch (Exception exception)
            {
                response.AddErrorMessage(exception.Message);
            }
            return response;
        }
        
        //deletes an appointment by id
        public async Task<ResponseModel> DeleteAppointment(int appointmentId)
        {
            var response = new ResponseModel();
            UnitOfWork.Delete(new Appointment() { Id = appointmentId });
            try
            {
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception exception)
            {

                response.AddErrorMessage(exception.Message);
            }
            return response;
        }
        
        //fetch all appointment from de beginning of the actual month 
        //example if 10/12/2016 it will fetch all appoinments from 01/12/2016 onwards 
        public async Task<ResponseModel> GetAppointments( )
        {
            var response = new ResponseModel();
           
            var date = DateTime.Now.AddDays(-30);
            try
            {
                var appoinments = await UnitOfWork.Table<Appointment>()
                                           .Where(x => x.IsDeleted == false)
                                           .Where(x => x.StartDate > date)
                                           //.Where(x => x.IsPaid == false)
                                           .Include(x => x.Patient)
                                           .ToListAsync();
                response.ExcecutedSuccesfully = true;
                response.SetResult(appoinments);
            }
            catch (Exception exception)
            {
                response.AddErrorMessage(exception.Message);
            }
            return response;
        }
        
        //ditto but of a specific dentist
        public async Task<ResponseModel> GetAppointmentsByDentistId(int dentistId)
        {
            var response = new ResponseModel();
            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            try
            {
                var appoinments = await UnitOfWork.Table<Appointment>()
                                           .Where(x => x.UserId == dentistId)
                                           .Where(x => x.IsDeleted == false)
                                           .Where(x => x.StartDate > date)
                                           .Include(x => x.Patient)
                                           .ToListAsync();
                response.ExcecutedSuccesfully = true;
                response.SetResult(appoinments);
            }
            catch (Exception exception)
            {
                response.AddErrorMessage(exception.Message);
            }
            return response;
        }
        
        //updates an appointment
        public async Task<ResponseModel> UpdateAppointment(int id, string startDate, string endDate, int patientId, int userId)
        {
            var response = new ResponseModel();
            Appointment appointment = new Appointment()
            {
                Id = id,
                StartDate = DateTime.ParseExact(startDate, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                EndDate = DateTime.ParseExact(endDate, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                UserId = userId,
                PatientId = patientId

            };
            try
            {
                UnitOfWork.Update(appointment);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.SetResult(appointment);
            }
            catch (Exception exception)
            {

                response.AddErrorMessage(exception.Message);
            }
            return response;
        }

        public async Task<ResponseModel<Appointment>> GetAppoimentById(int appoitmentId)
        {
            var result = new ResponseModel<Appointment>();

            try
            {
                var data = await UnitOfWork.Table<Appointment>()
                      .Include(x => x.Patient)
                      .Include(x => x.User)
                      .FirstOrDefaultAsync(x => x.Id.Equals(appoitmentId) && !x.IsPaid && !x.IsDeleted);

                if (data == null)
                {
                    result.AddErrorMessage("Este usuario no posee Citas Pendientes de pago");
                    return result;
                }

                result.Data = data;
            }
            catch (Exception exception)
            {
                result.AddErrorMessage(exception.Message);
                return result;
            }

            return result;
        }

        public async Task<ResponseModel<List<Appointment>>> GetAppointmentsByDateInterVal(DateTime firstInterval, DateTime lastInterval)
        {
            var response = new ResponseModel<List<Appointment>>();


            try
            {
                var appoinments = await UnitOfWork.Table<Appointment>()
                                            .Where(x => x.IsDeleted == false)
                                            .Where(x => x.StartDate > firstInterval)
                                            .Where(x => x.StartDate < lastInterval)
                                            .Include(x => x.Patient)
                                            .Include(x => x.Patient.Phones)
                                            .Include(x=>x.User)
                                            .OrderBy(x => x.StartDate)
                                            .ToListAsync();
                response.ExcecutedSuccesfully = true;
                response.Data = appoinments;
            }
            catch (Exception exception)
            {

                response.AddErrorMessage(exception.Message);
            }
            return response;
        }

        public async Task<ResponseModel> ConfirmAppointment(int appointmentId)
        {
            var response = new ResponseModel();
            try
            {
                var appointment = await UnitOfWork.Table<Appointment>().Where(x => x.Id == appointmentId).FirstOrDefaultAsync();
                appointment.IsConfirm = true;
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception exception)
            {

                response.AddErrorMessage(exception.Message);
            }
            return response;
        }
    }

   public interface IAppointmentService
    {
        Task<ResponseModel> CreateAppointment(string startDate, string endDate, int patientId, int userId);
        Task<ResponseModel> DeleteAppointment(int appointmentId);
        Task<ResponseModel> GetAppointments();
        Task<ResponseModel> GetAppointmentsByDentistId(int dentistId);
        Task<ResponseModel> UpdateAppointment(int id, string startDate, string endDate, int patientId, int userId);
        Task<ResponseModel<Appointment>> GetAppoimentById(int appoitmentId);
        Task<ResponseModel<List<Appointment>>> GetAppointmentsByDateInterVal(DateTime firstInterval, DateTime lastInterval);
        Task<ResponseModel> ConfirmAppointment(int appointmentId);
    }
}
