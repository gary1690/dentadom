﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Data.Entity;

namespace AppService.Services
{
    public class InventoryTransactionService : BaseService, IInventoryTransactionService
    {
        public InventoryTransactionService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> GetInventoryTransaction()
        {
            var response = new ResponseModel();
            try
            {
                var InventoryTransactions = await UnitOfWork.Table<InventoryTransaction>()
                                                            .OrderByDescending(x =>x.CreatedOn)
                                                            .Include(x => x.Product)
                                                            .ToListAsync();
                response.SetResult(InventoryTransactions);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception e)
            {
                response.SetResult(false);
                Console.WriteLine(e);
            }
            return response;
        }

        public async Task<ResponseModel> RollBackTransaction(int inventoryTransactionId)
        {
            var response = new ResponseModel();
            try
            {
                var invTransaction = await UnitOfWork.Table<InventoryTransaction>()
                                                .Where(x => x.Id == inventoryTransactionId)
                                                .Include(x => x.Product)
                                                .FirstOrDefaultAsync();
                if(invTransaction.Type == TransactionType.add)
                {
                    invTransaction.Product.Quantity -= invTransaction.Quantity;
                }
                if (invTransaction.Type == TransactionType.remove)
                {
                    invTransaction.Product.Quantity += invTransaction.Quantity;
                }

                UnitOfWork.Delete(invTransaction);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;

            }
            catch (Exception)
            {

            }

            return response;
        }

        public async Task<ResponseModel> UpdateTransaction(int id, int type, int quantity)
        {
            var response = new ResponseModel();
            try
            {
                var invTransaction = await UnitOfWork.Table<InventoryTransaction>()
                                                .Where(x => x.Id == id)
                                                .Include(x => x.Product)
                                                .FirstOrDefaultAsync();
                if (invTransaction.Type == TransactionType.add)
                {
                    var diferencia = quantity - invTransaction.Quantity;
                    invTransaction.Quantity += diferencia;
                    invTransaction.Product.Quantity += diferencia;
                }
                if (invTransaction.Type == TransactionType.remove)
                {
                    var diferencia = quantity - invTransaction.Quantity;
                    invTransaction.Quantity += diferencia;
                    invTransaction.Product.Quantity -= diferencia;
                }

                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;

            }
            catch (Exception)
            {

            }

            return response;
        }
    }

    public interface IInventoryTransactionService
    {
        Task<ResponseModel> GetInventoryTransaction();
        Task<ResponseModel> RollBackTransaction(int inventoryTransactionId);
        Task<ResponseModel> UpdateTransaction(int id, int type, int quantity);
    }
}
