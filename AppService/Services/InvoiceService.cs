﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Transactions;
using System.Threading.Tasks;
using AppService.Framework;
using Data.Entities;
using Data.Framework;
using Domain.Global.Dto;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using Reports.Helper;
using Reports.Reports.Invoice;

namespace AppService.Services
{
    public class InvoiceService : BaseService, IInvoiceService
    {
        public InvoiceService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel<int>> CreateInvoiceAsync(Invoice invoice)
        {
            var result = new ResponseModel<int>();

            if (invoice.InvoiceDetails == null || !invoice.InvoiceDetails.Any())
            {
                result.AddErrorMessage("Ocurrio un Error tratando de Crear la Factura");
                return result;
            }
            try
            {
                using (var transactionScop = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    UnitOfWork.Table<Invoice>().Add(invoice);
                    await UnitOfWork.SaveChangesAsync();

                    var appointmet = await UnitOfWork.Table<Appointment>().FirstOrDefaultAsync(x => x.Id.Equals(invoice.AppointmentId));

                    if (appointmet == null)
                    {
                        result.AddErrorMessage("Ocurrio un error tratando de actualizar el pago de la Cita");
                        return result;
                    }

                    appointmet.IsPaid = true;

                    UnitOfWork.Update(appointmet);
                    await UnitOfWork.SaveChangesAsync();

                    transactionScop.Complete();
                    result.Data = invoice.Id;
                }
               
            }
            catch (Exception exception)
            {
                result.AddErrorMessage(exception.Message);
                return result;
            }

            return result;
        }

        public async Task<ResponseModel<Invoice>> GetInvoiceByAppointmentId(int appointmentId)
        {
            var result = new ResponseModel<Invoice>();
            try { 
            var data = await UnitOfWork.Table<Invoice>().Where(x => x.AppointmentId == appointmentId).FirstOrDefaultAsync();
            result.Data = data;
            }
            catch (Exception)
            {
                result.AddErrorMessage("No se encontro la factura");
            }
            return result;
        }

        public async  Task<ResponseModel<List<Invoice>>> GetInvoiceByDateInterval(DateTime firstInterval, DateTime lastInterval)
        {
            var response = new ResponseModel<List<Invoice>>();
            try
            {
                var invoices = await UnitOfWork.Table<Invoice>()
                                            //.Where(x => x.CreatedOn > firstInterval)
                                            //.Where(x => x.CreatedOn < lastInterval)
                                            .Include(x => x.InvoiceDetails)
                                            .Include(x => x.Appointment)
                                            .Where(x => x.Appointment.StartDate > firstInterval)
                                            .Where(x => x.Appointment.StartDate < lastInterval)
                                            .ToListAsync();
                response.ExcecutedSuccesfully = true;
                response.Data = invoices;
            }
            catch (Exception exception)
            {

                response.AddErrorMessage(exception.Message);
            }
            return response;
        }

        public async Task<ResponseModel<Invoice>> GetInvoiceById(int invoiceId)
        {
            var result = new ResponseModel<Invoice>();
            var data = await UnitOfWork.Table<Invoice>().Include(x => x.User)
                .Include(x => x.Patient).Include(x => x.Patient.Phones)
                .Include(x => x.InvoiceDetails.Select(y => y.MedicalProcedure))
                .Include(x => x.Appointment).Where(x => x.Id == invoiceId).FirstOrDefaultAsync();

            result.Data = data;

            return result;
        }

        public async Task<ResponseModel<List<InvoiceReportDto>>> GetByDatesInterval(InvoiceReportDto criteria)
        {
            var result = new ResponseModel<List<InvoiceReportDto>>();

            criteria.ToDateAsDateTime = Convert.ToDateTime(criteria.ToDate, CultureInfo.InvariantCulture);
            criteria.FromDateAsDateTime = Convert.ToDateTime(criteria.FromDate, CultureInfo.InvariantCulture);

            try
            {
                var data = await UnitOfWork.Table<InvoiceDetail>()
                    .Include(x => x.Invoice.Patient)
                    .Include(x => x.Invoice)
                    .Include(x => x.MedicalProcedure)
                    .Where(x => !x.Invoice.IsDeleted)
                    .Where(x => x.Invoice.CreatedOn >= criteria.FromDateAsDateTime && x.Invoice.CreatedOn <= criteria.ToDateAsDateTime)
                    .Select(x => new InvoiceReportDto
                    {
                        InvoiceNumber = x.Invoice.Id.ToString(),
                        InvoiceAsDateTime = x.CreatedOn,
                        CustomerName = x.Invoice.Patient.Name + " " + x.Invoice.Patient.LastName,
                        Price = x.MedicalProcedurePrice,
                        Discount = x.Invoice.Discount,
                        Tax = x.Invoice.Tax,
                        ClinicalProcedure = x.MedicalProcedure.Description,
                        FromDate = criteria.FromDate,
                        ToDate = criteria.ToDate
                    })
                    .ToListAsync();



                result.Data = data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return result;
        }

        public async Task<ReportResponse> GetInvoiceReport<TData>(TData data) where TData : class
        {
            var response = ReportGenerator.GetReportFile<InvoiceReport, TData>(data);

            return response;
        }

        public async Task<ReportResponse> GetDetailedInvoiceReport<TData>(TData data) where TData : class
        {
            var response = ReportGenerator.GetReportFile<InvoiceReportDetailed, TData>(data);

            return response;
        }
    }

    public interface IInvoiceService : IBaseService
    {
        Task<ResponseModel<int>> CreateInvoiceAsync(Invoice invoice);
        Task<ResponseModel<Invoice>> GetInvoiceById(int invoiceId);
        Task<ResponseModel<Invoice>> GetInvoiceByAppointmentId(int appointmentId);
        Task<ResponseModel<List<Invoice>>> GetInvoiceByDateInterval(DateTime firstInterval, DateTime lastInterval);
        Task<ResponseModel<List<InvoiceReportDto>>> GetByDatesInterval(InvoiceReportDto criteria);
        Task<ReportResponse> GetInvoiceReport<TData>(TData data) where TData : class;
        Task<ReportResponse> GetDetailedInvoiceReport<TData>(TData data) where TData : class;
    }
}
