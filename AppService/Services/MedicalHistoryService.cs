﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Data.Entity;

namespace AppService.Services
{
    public class MedicalHistoryService : BaseService, IMedicalHistoryService
    {
        public MedicalHistoryService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> GetorCreateMedicalByPatientId(int patientId)
        {
            var response = new ResponseModel();
            try
            {

                var medicalHistory = new MedicalHistory();
                if (!UnitOfWork.Table<MedicalHistory>().Any(x => x.PatientId == patientId))
                {
                    medicalHistory = new MedicalHistory() {PatientId = patientId };
                    UnitOfWork.Add(medicalHistory);
                    await UnitOfWork.SaveChangesAsync();
                }

                medicalHistory = await UnitOfWork.Table<MedicalHistory>()
                                                .Where(x => x.PatientId == patientId)
                                                .Include(x => x.Patient)
                                                .Include(x => x.Patient.Phones)
                                                .Include(x => x.Treatments)
                                                .Include(x => x.Conditions)
                                                .Include(x => x.Medications)
                                                .FirstOrDefaultAsync();
                response.SetResult(true);
                response.SetResult(medicalHistory);
            }
            catch(Exception e)
            {
                response.SetResult(false);
                Console.WriteLine(e);
            }
            return response;
        }

        public async Task<ResponseModel> Update(int id, int patientId, int pDI, int gI, int plI, int sHI)
        {
            var response = new ResponseModel();
            var medicalHistory = new MedicalHistory() {
                Id =id,
                PatientId =patientId,
                PDI = pDI,
                GI = gI,
                PlI = plI,
                SHI =sHI
            };
            try
            {
                UnitOfWork.Update(medicalHistory);
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(medicalHistory);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }
            
            return response;
        }
    }

    public interface IMedicalHistoryService
    {
        Task<ResponseModel> GetorCreateMedicalByPatientId(int patientId);
        Task<ResponseModel> Update(int id, int patientId, int pDI, int gI, int plI, int sHI);
    }
}
