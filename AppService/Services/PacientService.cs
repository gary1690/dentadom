﻿using System;
using System.Collections.Generic;
using AppService.Framework;
using Data.Entities;
using Data.Framework;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace AppService.Services
{
    public class PatientService : BaseService, IPatientService
    {
        public PatientService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        //creates a patient with his phone number
        public async Task<ResponseModel> CreatePatient(string name, string lastName, string birthday, string phoneNumber, string phoneType, string email)
        {
            var response = new ResponseModel();
            try
            {
                var patient = new Patient()
                {
                    Name = name,
                    LastName = lastName,
                    Birthday = DateTime.Parse(birthday, new CultureInfo("en-CA")),
                    Email = email
                };
                patient.Phones = new List<Phone>() {
                new Phone()
                {
                    Number = phoneNumber,
                    Type = phoneType,
                    Primary = 1
                }
            };
                UnitOfWork.Add(patient);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.SetResult(patient);
            }
            catch (Exception)
            {

                response.ExcecutedSuccesfully = false;
            }
            return response;

        }
        
        //fetch a patient from database by Id
        public async Task<ResponseModel> GetPatient(int id)
        {
            var response = new ResponseModel();

            try
            {
                var patient = await UnitOfWork.Table<Patient>().Where(x => x.Id == id).Include(x => x.Phones).FirstOrDefaultAsync();
                response.ExcecutedSuccesfully = true;
                response.SetResult(patient);
            }
            catch (Exception e)
            {
               
                response.ExcecutedSuccesfully = false;
            }
            return response;
        }

        //fetch all the patients
        public async Task<ResponseModel> GetPatients()
        {
            var response = new ResponseModel();
            var patients = await UnitOfWork.Table<Patient>().Include(x => x.Phones).ToListAsync();
            response.SetResult(true);
            response.SetResult(patients);
            return response;
        }

        public async Task<ResponseModel> UpdatePatient(int id, string name, string lastName, string birthday, string phoneNumber, string phoneType, string email)
        {
            var response = new ResponseModel();
            try
            {
                var patient = new Patient()
                {
                    Id = id,
                    Name = name,
                    LastName = lastName,
                    Birthday = DateTime.Parse(birthday, new CultureInfo("en-CA")),
                    Email = email,
                };
                UnitOfWork.Update(patient);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {

                response.ExcecutedSuccesfully = false;
            }
            return response;
            
        }
    }

    public interface IPatientService
    {
        Task<ResponseModel> GetPatient(int id);
        Task<ResponseModel> GetPatients();
        Task<ResponseModel> CreatePatient(string name, string lastName, string birthday, string phoneNumber, string phoneType, string email);
        Task<ResponseModel> UpdatePatient(int id, string name, string lastName, string birthday, string phoneNumber, string phoneType, string email);
    }
}
