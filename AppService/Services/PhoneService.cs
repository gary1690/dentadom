﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Data.Entity;

namespace AppService.Services
{
    public class PhoneService : BaseService, IPhoneService
    {
        public PhoneService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> updatePhone(int id, string phoneNumber, string phoneType)
        {
            var response = new ResponseModel();
            var phone = await UnitOfWork.Table<Phone>().Where(x => x.PatientId == id).FirstOrDefaultAsync() ?? new Phone();
            phone.Number = phoneNumber;
            phone.Type = phoneType;
            await UnitOfWork.SaveChangesAsync();
            return response;
        }
    }

    public interface IPhoneService
    {
        Task<ResponseModel> updatePhone(int id, string phoneNumber, string phoneType);
    }
}
