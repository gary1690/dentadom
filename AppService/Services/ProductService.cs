﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Data.Entity;

namespace AppService.Services
{
    public class ProductService : BaseService, IProductService
    {
        public ProductService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> AddProduct(int productId, int quantity, int type)
        {
            var response = new ResponseModel();
            try
            {
                if (UnitOfWork.Table<Product>().Any(x => x.Id == productId))
                {
                    var product = await UnitOfWork.Table<Product>().Where(x => x.Id == productId).FirstOrDefaultAsync();
                    product.Quantity = product.Quantity + quantity;
                    var inventoryTransaction = new InventoryTransaction()
                    {
                        ProductId = productId,
                        Quantity = quantity,
                        Type = (TransactionType)type
                    };
                    UnitOfWork.Add(inventoryTransaction);
                    await UnitOfWork.SaveChangesAsync();
                    response.SetResult(product);
                    response.ExcecutedSuccesfully = true;
                }

            }
            catch (Exception)
            {

            }

            return response;
        }

        public async Task<ResponseModel> CreateProduct(string name, string brand, int supplierId, double unitCost, int quantity)
        {
           var response = new ResponseModel();
            try
            {
                var product = new Product()
                {
                    Name = name,
                    Brand =brand,
                    SupplierId =supplierId,
                    UnitCost = unitCost,
                    Quantity =quantity
                };
                UnitOfWork.Add(product);
                await UnitOfWork.SaveChangesAsync();
                var inventoryTransaction = new InventoryTransaction()
                {
                    ProductId = product.Id,
                    Quantity = quantity,
                    Type = (TransactionType)2
                };
                UnitOfWork.Add(inventoryTransaction);
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(product);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {
                
            }

           return response;
        }

        public async Task<ResponseModel> GetProducts()
        {
            var response = new ResponseModel();
            try
            {
                var products = await UnitOfWork.Table<Product>().Include(x => x.Supplier).ToListAsync();
                response.SetResult(products);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {

            }

            return response;
        }

        public async Task<ResponseModel> RemoveProduct(int productId, int quantity, int type)
        {
            var response = new ResponseModel();
            try
            {
                if(UnitOfWork.Table<Product>().Any(x => x.Id == productId))
                {
                    var product =await UnitOfWork.Table<Product>().Where(x => x.Id == productId).FirstOrDefaultAsync();
                    product.Quantity = product.Quantity - quantity;
                    var inventoryTransaction = new InventoryTransaction()
                    {
                        ProductId = productId,
                        Quantity = quantity,
                        Type = (TransactionType)type
                    };
                    UnitOfWork.Add(inventoryTransaction);
                    await UnitOfWork.SaveChangesAsync();
                    response.SetResult(product);
                    response.ExcecutedSuccesfully = true; 
                }
                
            }
            catch (Exception)
            {

            }

            return response;
        }
    }

    public interface IProductService
    {
        Task<ResponseModel> CreateProduct(string name, string brand, int supplierId, double unitCost, int quantity);
        Task<ResponseModel> AddProduct(int productId, int quantity, int type);
        Task<ResponseModel> RemoveProduct(int productId, int quantity, int type);
        Task<ResponseModel> GetProducts();
    }
}
