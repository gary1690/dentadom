﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AppService.Framework;
using Data.Entities;
using Data.Framework;

namespace AppService.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel<User>> CreateAsync(User user)
        {
            var result = new ResponseModel<User>();

            var isExists = await UnitOfWork.Table<User>().FirstOrDefaultAsync(x => x.UserName.Equals(user.UserName));

            if (isExists != null)
            {
                result.AddErrorMessage("Existe un Usuario con este Nombre de Usuario(UserName)");
                return result;
            }
            
            var userToCreate = new User
            {
                Name = user.Name,
                LastName = user.LastName,
                UserName = user.UserName,
                Password = user.Password,
                Photo = user.Photo,
                Image = user.Image,
                ImageName = user.ImageName
            };

            if (user.Roles != null)
            {
                var newRoles = new List<Role>();
                foreach (var role in user.Roles)
                {
                    newRoles.Add(await UnitOfWork.Table<Role>().Where(x => x.Id == role.Id).FirstOrDefaultAsync());
                }
                userToCreate.Roles = newRoles;
            }

            try
            {
                UnitOfWork.Add(userToCreate);
               
                await UnitOfWork.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                result.AddErrorMessage(exception.Message);
                return result;
            }
            return result;
        }

        public async Task<ResponseModel<User>> UpdateAsync(User user)
        {
            var result = new ResponseModel<User>();

            var userToUpdate = await UnitOfWork.Table<User>().Where(x=> x.Id == user.Id).Include(x => x.Roles).FirstOrDefaultAsync( );
            if (userToUpdate == null)
            {
                result.AddErrorMessage("El Usuario que intenta Actualizar no existe");
                return result;
            }
            if (user.Roles != null)
            {
                var newRoles = new  List<Role>();
                foreach (var role in user.Roles)
                {
                   newRoles.Add( await UnitOfWork.Table<Role>().Where(x => x.Id == role.Id).FirstOrDefaultAsync());
                }
                userToUpdate.Roles = newRoles;
            }
            if(user.Photo != null)
            {
                userToUpdate.Image = user.Image;
                userToUpdate.ImageName = user.ImageName;
                userToUpdate.Photo = user.Photo;
            }
            userToUpdate.Name = user.Name;
            userToUpdate.LastName = user.LastName;
            userToUpdate.UserName = user.UserName;
            userToUpdate.Password = user.Password;
            
            try
            {

                UnitOfWork.Update(userToUpdate);
                await UnitOfWork.SaveChangesAsync();
                result.Data = userToUpdate;
            }
            catch (Exception exception)
            {
                result.AddErrorMessage(exception.Message);
                return result;
            }
            return result;
        }

        public async Task<ResponseModel<Branch>> GetBranchAsync()
        {
            var result = new ResponseModel<Branch>();

            var branch = await UnitOfWork.Table<Branch>().FirstOrDefaultAsync();

            result.Data = branch;

            return result;
        }

        

        public async Task<ResponseModel<List<User>>> GetAllAsync()
        {
            var result = new ResponseModel<List<User>>();

            var users = await UnitOfWork.Table<User>().Where(x => !x.IsDeleted).ToListAsync();

            result.Data = users;

            return result;
        }

        public async Task<ResponseModel<User>> GetUserById(int id)
        {
            var result = new ResponseModel<User>();

            try
            {
             
                var data = await UnitOfWork.Table<User>().Where(x => x.Id == id).Include(x => x.Roles).FirstOrDefaultAsync( );
                if (data == null)
                {
                    result.AddErrorMessage("El Usuario que busca no existe");
                    return result;
                }
                result.ExcecutedSuccesfully = true;
                result.Data = data;

             
            }
            catch (Exception exception)
            {

                result.AddErrorMessage(exception.Message);
                return result;
            }

            return result;
        }

        public async Task<ResponseModel<List<Role>>> GetAllRoles()
        {
            var result = new ResponseModel<List<Role>>();

            try
            {
                var data = await UnitOfWork.Table<Role>().ToListAsync();

                result.Data = data;

                return result;
            }
            catch (Exception exception)
            {
                result.AddErrorMessage(exception.Message);
                return result;
            }
        }

        
        public async Task<ResponseModel> GetUser(string userName, string password)
        {
            var response = new ResponseModel();

            var Dbset = UnitOfWork.Table<User>();

            var user = await Dbset.Where(x => x.UserName == userName)
                .Where(x => x.Password == password)
                .Include(x => x.Roles)
                .Where(x => !x.IsDeleted)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                response.AddErrorMessage("Usuaio/Contrasena Invalido");
                return response;
            }

            response.SetResult(user);
            return response;
        }
        
        //fetch all dentist in the data base, 
        //thinking of doing another service for this not sure
        public async Task<ResponseModel> getDentists()
        {
            var response = new ResponseModel();
            try
            {
                var alldoctors = (await UnitOfWork.Table<Role>()
                                  .Where(x => x.Description == "Odontologo")
                                  .Include(x => x.Users)
                                  .FirstAsync()).Users;
                var doctors = alldoctors.Where(x => !x.IsDeleted);
                response.ExcecutedSuccesfully = true;
                response.SetResult(doctors);
            }
            catch (Exception)
            {

                response.ExcecutedSuccesfully = false;
            }
            return response;

        }

        public async Task<ResponseModel> setUserPreferences(int id, string preferences)
        {
            var response = new ResponseModel();
            try
            {
                var User = await UnitOfWork.Table<User>()
                                   .Where(x => x.Id == id)
                                   .FirstOrDefaultAsync();
                User.Preferences = preferences;
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(true);
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }

        public async Task<ResponseModel> Delete(int id)
        {
            var response = new ResponseModel();
            try
            {
                var user = UnitOfWork.Table<User>()
                                    .Where(x => x.Id == id)
                                    .FirstOrDefault();
                user.IsDeleted = true;
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception e)
            {
                throw e;
                response.AddErrorMessage("No se pudo borrar al usuario");
            }
            return response;
            
        }
    }

    public interface IUserService : IBaseService
    {
        Task<ResponseModel> GetUser(string userName, string password);
        Task<ResponseModel<User>> CreateAsync(User user);
        Task<ResponseModel<List<User>>> GetAllAsync();
        Task<ResponseModel<User>> GetUserById(int id);
        Task<ResponseModel<User>> UpdateAsync(User user);
        Task<ResponseModel> getDentists();
        Task<ResponseModel> setUserPreferences(int id, string preferences);
        Task<ResponseModel<List<Role>>> GetAllRoles();
        Task<ResponseModel<Branch>> GetBranchAsync();
        Task<ResponseModel> Delete(int id);
    }
}
