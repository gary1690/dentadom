﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;

namespace Data
{
    public class DentaDomContext : DbContext
    {
        public DentaDomContext() : base("name=DentaDomConnString")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new NullDatabaseInitializer<DentaDomContext>());
            Database.Log = m => Debug.WriteLine(m);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Configurations.AddFromAssembly(typeof(DentaDomContext).Assembly);
        }
    }
}
