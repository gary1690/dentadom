﻿using System;
using Data.Framework;
using Data.Validations;

namespace Data.Entities
{
    public class Appointment : ValidateSoftDeleteEntity<AppointmentValidator>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
        public bool IsPaid { get; set; } = false;
        public bool IsConfirm { get; set; } = false;
    }
}
