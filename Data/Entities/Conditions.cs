﻿using Data.Framework;
using Data.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Condition : ValidateEntity <CondictionValidator>
    {
        public ConditionType Type { get; set; }
        public string Description { get; set;}
        public int MedicalHistoryId { get; set;}
        public MedicalHistory MedicalHistory { get; set;}
    }

    public enum ConditionType
    {
        [Display(Name = "Alergia")]
        Allergy = 1,
        [Display(Name = "Enfermedad")]
        disease
    }
}
