﻿using Data.Framework;
using Data.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class InventoryTransaction : ValidateEntity<InventoryTransactionValidator>
    {
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int Quantity { get; set; }
        public TransactionType Type { get; set;}
    }

    public enum TransactionType
    {
        remove = 1,
        add
    }
}
