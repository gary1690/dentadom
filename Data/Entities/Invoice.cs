﻿using System.Collections.Generic;
using Data.Framework;
using Data.Validations;
using Domain.Global.Enums;

namespace Data.Entities
{
    public class Invoice : ValidateSoftDeleteEntity<InvoiceValidator>
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public decimal Tax { get; set; } = 0;
        public decimal Discount { get; set; } = 0;
        public Patient Patient { get; set; }
        public int PatientId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public int AppointmentId { get; set; }
        public Appointment Appointment { get; set; }
        public ICollection<InvoiceDetail> InvoiceDetails { get; set; }
    }
}
