﻿
using System.Collections.Generic;
using Data.Framework;
using Data.Validations;
using Domain.Global.Enums;

namespace Data.Entities
{
    public class MedicalProcedure : ValidateSoftDeleteEntity<MedicalProcedureValidator>
    {
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string DescriptionAndPrice => $"{Description}   -  {Price}";
        public ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public MedicalProcedureType MedicalProcedureType { get; set; }
    }
}
