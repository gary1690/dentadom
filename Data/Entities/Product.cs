﻿using Data.Framework;
using Data.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Product : ValidateEntity<ProductValidator>
    {
        public string Name { get; set; }
        public string Brand{ get; set; }
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
        public double UnitCost { get; set; }
        public int Quantity { get; set; }
    }
}
