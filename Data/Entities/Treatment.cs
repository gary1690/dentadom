﻿using Data.Framework;
using Data.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Treatment : ValidateSoftDeleteEntity<TreatmentValidator>
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public byte[] Attachment { get; set; }
        public string AttachmentName { get; set; }
        public int MedicalHistoryId { get; set; }
        public MedicalHistory MedicalHistory { get; set; }

    }
}
