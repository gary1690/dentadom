﻿using Data.Framework;
using Data.Validations;
using System.Collections.Generic;
using Domain.Global.Enums;

namespace Data.Entities
{
    public class User : ValidateSoftDeleteEntity<UserValidator>
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Preferences { get; set; }
        public string Photo { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual List<Appointment> Appointments { get; set; }
        public byte[] Image { get; set; }
        public string ImageName { get; set; }
        public string CompleteName => $"{Name} {LastName}";
    }
}
