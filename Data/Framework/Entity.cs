﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Framework
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
    }
}
