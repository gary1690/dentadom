﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data.Framework
{
    public interface IUnitOfWork : IDisposable
    {
        void Add<TEntity>(TEntity entity) where TEntity : Entity;
        void Update<TEntity>(TEntity entity) where TEntity : Entity;
        void Delete<TEntity>(TEntity entity) where TEntity : Entity;
        IDbSet<TEntity> Table<TEntity>() where TEntity : Entity;
        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}
