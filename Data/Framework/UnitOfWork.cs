﻿using System.Data.Entity;
using System.Threading.Tasks;

namespace Data.Framework
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DentaDomContext _dentaDomContext;

        public UnitOfWork(DentaDomContext dentaDomContext)
        {
            _dentaDomContext = dentaDomContext;
        }

        public void Add<TEntity>(TEntity entity) where TEntity : Entity
        {
            _dentaDomContext.Set<TEntity>().Add(entity);
        }

        public void Update<TEntity>(TEntity entity) where TEntity : Entity
        {
            _dentaDomContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : Entity
        {
            _dentaDomContext.Entry(entity).State = EntityState.Deleted;
        }

        public IDbSet<TEntity> Table<TEntity>() where TEntity : Entity
        {
            return _dentaDomContext.Set<TEntity>();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dentaDomContext.SaveChangesAsync();
        }
        public int SaveChanges()
        {
            return _dentaDomContext.SaveChanges();
        }
        public void Dispose()
        {
            _dentaDomContext?.Dispose();
        }
    }
}
