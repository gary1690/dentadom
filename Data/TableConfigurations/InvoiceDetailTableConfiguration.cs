﻿using System.Data.Entity.ModelConfiguration;
using Data.Entities;

namespace Data.TableConfigurations
{
    public class InvoiceDetailTableConfiguration : EntityTypeConfiguration<InvoiceDetail>
    {
        public InvoiceDetailTableConfiguration()
        {
            HasRequired(x => x.Invoice).WithMany(x => x.InvoiceDetails);
        }
    }
}
