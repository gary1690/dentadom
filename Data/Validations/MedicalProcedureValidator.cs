﻿using Data.Entities;
using FluentValidation;

namespace Data.Validations
{
    public class MedicalProcedureValidator : AbstractValidator<MedicalProcedure>
    {
        public MedicalProcedureValidator()
        {
            RuleFor(x => x.Description).NotEmpty();
            //RuleFor(x => x.MedicalProcedureType).NotEmpty();
        }
    }
}