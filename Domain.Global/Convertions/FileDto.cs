﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Global.Convertions
{
    public class FileDto
    {
        public byte[] File { get; set; }
        public string FileName { get; set; }
    }
}
