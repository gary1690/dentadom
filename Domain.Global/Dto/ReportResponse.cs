﻿namespace Domain.Global.Dto
{
    public class ReportResponse
    {
        public byte[] FileContent { get; set; }
    }
}
