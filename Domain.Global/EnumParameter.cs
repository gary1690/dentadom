﻿namespace Domain.Global
{
    public class EnumParameter
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
    }
}
