﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Global.Enums
{
    public enum PaymentMethod
    {
        [Display (Name = "Efectivo")]
        Cash = 1,
        [Display(Name = "A Credito")]
        Credit
    }
}
