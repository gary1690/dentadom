﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Domain.Global
{
    public class Mapper
    {
        public static TType MapTo<TType>(dynamic fromObject, TType toObject) where TType : class
        {
            if (fromObject == null)
            {
                throw new Exception("Error intentado Mapear, debe especificar tipo a Retornor");
            }

            var toObjecType = typeof(TType);

            Type[] typeArgs = { typeof(TType)};

            var makeme = toObjecType.MakeGenericType(typeArgs);

            var typeToReturn = Activator.CreateInstance(makeme);


            var fromObjectProps = fromObject.GetProperties(BindingFlags.Public | BindingFlags.Instance);


            // set property values using reflection
            var values = (List<TType>)fromObject;
            foreach (var prop in fromObjectProps)
            {
                foreach (var typetoR in (IEnumerable)typeToReturn)
                {
                    if (((PropertyInfo)typetoR).Name.Equals(prop.Name))
                    {
                        prop.SetValue(typeToReturn, values[prop.Name]);
                    }   
                }
            }
            return (TType)typeToReturn;
        }
    }
}
