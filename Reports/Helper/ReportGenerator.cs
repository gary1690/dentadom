﻿using System;
using System.IO;
using DevExpress.XtraReports.UI;
using Domain.Global.Dto;

namespace Reports.Helper
{
    public class ReportGenerator
    {
        private static XtraReport BuidReport<TXtraReport, TData>(TData data) where TXtraReport : XtraReport
            where TData : class
        {
            var reportdata = data;

            var reportInstance = Activator.CreateInstance<TXtraReport>();

            if (data != null)
                reportInstance.DataSource = reportdata;

            return reportInstance;
        }

        public static ReportResponse GetReportFile<TXtraReport, TData>(TData data) where TXtraReport : XtraReport
            where TData : class
        {
            var report = BuidReport<TXtraReport, TData>(data);

            var memoryStream = new MemoryStream();

            try
            {
                report.ExportToPdf(memoryStream);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            var response = new ReportResponse
            {
                FileContent = memoryStream.ToArray()
            };
            return response;
        }
    }
}
