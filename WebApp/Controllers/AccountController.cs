﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using AppService.Services;
using WebApp.Helpers;
using WebApp.ViewModels;
using WebApp.ViewModels.User;
using System.Web;

namespace WebApp.Controllers
{
    //thinking of UserController
    //Made to control al user Pages and User Json feed, also login & logout
    public class AccountController : BaseController
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService) : base(userService)
        {
            _userService = userService;
        }

        [AuthenticationNoRequired]//method name says it all
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]//method name says it all
        [AuthenticationNoRequired]//method name says it all
        public async Task<ActionResult> Login(UserLoginViewModel user)
        {
            if (!ModelState.IsValid) return View();
            var response = await _userService.GetUser(user.UserName, user.Password);
            if (response.ExcecutedSuccesfully)
            {
                var id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
                SessionHelper.AddUserToSession(id.ToString());
                SessionHelper.setUserInfo(response);
                return Redirect("~/");
            }
            return View();
        }

        [AuthenticationRequired]//ditto
        public ActionResult Logout( )
        {
            SessionHelper.DestroyUserSession();
            return Redirect("~/Account/Login");
        }

        [AuthenticationRequired]//ditto
        public async Task<JsonResult> SetUserPreferences(string preferences)
        {
            var respond = await _userService.setUserPreferences(SessionHelper.GetUser(), preferences);
            SessionHelper.setUserPreferences(preferences);
            return Json(respond, JsonRequestBehavior.AllowGet);
        }

      

        //provide a json feed of all the dentist -> user related feed
        public async Task<JsonResult> DentistFeed()
        {
            var response = await _userService.getDentists();
            var dentists = new List<JsonFeedDentistViewModel>();
            if (response.ExcecutedSuccesfully)
            {
                foreach(var denitst in response.Result)
                {
                    var aDentist = new JsonFeedDentistViewModel()
                    {
                        Id = denitst.GetType().GetProperty("Id").GetValue(denitst, null),
                        Name = denitst.GetType().GetProperty("Name").GetValue(denitst, null),
                        LastName = denitst.GetType().GetProperty("LastName").GetValue(denitst, null)
                    };
                    dentists.Add(aDentist);
                }
                
            }
            return Json(dentists,JsonRequestBehavior.AllowGet);
        }

       
    }
}