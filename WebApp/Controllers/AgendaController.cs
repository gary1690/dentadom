﻿using AppService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.ViewModels;
using Data.Entities;
using AppService.Framework;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    //Thinking of calling Appointment Controller
    //Agenda Views & Appointment related Json feed
    [AuthenticationRequired]
    public class AgendaController : BaseController
    {

        private readonly IAppointmentService _appointmentService;
        private readonly IPatientService _patientService;

        public AgendaController(IAppointmentService appointmentService, IPatientService patientService)
        {
            _appointmentService = appointmentService;
            _patientService = patientService;
        }
        //Make Appoinment
        [ReferrOnly]
        //[Secretary]
        //[CheckAccess]
        public async Task<ActionResult> MakeAppointment( int id)
        {
            var response = await _patientService.GetPatient(id);
            if (response.ExcecutedSuccesfully) {
                var patient = response.Result;
                JsonFeedPatientViewModel selectedPatients = new JsonFeedPatientViewModel()
                {
                    Id = patient.GetType().GetProperty("Id").GetValue(patient, null),
                    Name = patient.GetType().GetProperty("Name").GetValue(patient, null),
                    LastName = patient.GetType().GetProperty("LastName").GetValue(patient, null),
                    Birthday = (patient.GetType().GetProperty("Birthday").GetValue(patient, null)).ToString("dd'/'MM'/'yyyy"),
                    Email = patient.GetType().GetProperty("Email").GetValue(patient, null)
                };
                var phones = patient.GetType().GetProperty("Phones").GetValue(patient, null);
                foreach (var phone in phones)
                {
                    if (phone.GetType().GetProperty("Primary").GetValue(phone, null) == 1)
                    {
                        selectedPatients.PhoneNumber = phone.GetType().GetProperty("Number").GetValue(phone, null);
                        selectedPatients.PhoneType = phone.GetType().GetProperty("Type").GetValue(phone, null);
                    }

                }
                return View(selectedPatients);
            }
            return View();
        }
        //Manage schedule
        //[Secretary]
        //[CheckAccess]
        public ActionResult Scheduler()
        {
            return View();
        }

        [Dentist]
        [CheckAccess]
        public ActionResult DentistAgenda()
        {
            ViewBag.UserId = SessionHelper.GetUser();
            return View();
        }

        public async Task<JsonResult> AppointmentsFeed( )
        {
            var response = await _appointmentService.GetAppointments( ).ConfigureAwait(false);
            var appointments = new List<JsonFeedAppointmentViewModel>();
            foreach(var appoinment in response.Result)
            {
                var patient = appoinment.GetType().GetProperty("Patient").GetValue(appoinment, null);
                appointments.Add(new JsonFeedAppointmentViewModel()
                {
                    Id = appoinment.GetType().GetProperty("Id").GetValue(appoinment, null),
                    StartDate =(appoinment.GetType().GetProperty("StartDate").GetValue(appoinment, null)).ToString("yyyy-MM-ddTHH:mm:ss"),
                    EndDate = (appoinment.GetType().GetProperty("EndDate").GetValue(appoinment, null)).ToString("yyyy-MM-ddTHH:mm:ss"),
                    Description = appoinment.GetType().GetProperty("Description").GetValue(appoinment, null),
                    UserId = appoinment.GetType().GetProperty("UserId").GetValue(appoinment, null),
                    PatientId = appoinment.GetType().GetProperty("PatientId").GetValue(appoinment, null),
                    IsPaid = appoinment.GetType().GetProperty("IsPaid").GetValue(appoinment, null),
                    PatientName = patient.GetType().GetProperty("Name").GetValue(patient, null) + " "+
                                    patient.GetType().GetProperty("LastName").GetValue(patient, null)

                });
            }
              
           
            return Json(appointments, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AppointmentsFeedByDentistId(int dentistId)
        {
            var response = await _appointmentService.GetAppointmentsByDentistId(dentistId).ConfigureAwait(false);
            var appointments = new List<JsonFeedAppointmentViewModel>();
            foreach (var appoinment in response.Result)
            {
                var patient = appoinment.GetType().GetProperty("Patient").GetValue(appoinment, null);
                appointments.Add(new JsonFeedAppointmentViewModel()
                {
                    Id = appoinment.GetType().GetProperty("Id").GetValue(appoinment, null),
                    StartDate = (appoinment.GetType().GetProperty("StartDate").GetValue(appoinment, null)).ToString("yyyy-MM-ddTHH:mm:ss"),
                    EndDate = (appoinment.GetType().GetProperty("EndDate").GetValue(appoinment, null)).ToString("yyyy-MM-ddTHH:mm:ss"),
                    Description = appoinment.GetType().GetProperty("Description").GetValue(appoinment, null),
                    UserId = appoinment.GetType().GetProperty("UserId").GetValue(appoinment, null),
                    PatientId = appoinment.GetType().GetProperty("PatientId").GetValue(appoinment, null),
                    PatientName = patient.GetType().GetProperty("Name").GetValue(patient, null) + " " +
                                    patient.GetType().GetProperty("LastName").GetValue(patient, null)

                });
            }


            return Json(appointments, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> CancelAppointment(int appointmentId)
        {
            
            var response = await _appointmentService.DeleteAppointment(appointmentId);
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public async Task<JsonResult> ConfirmAppointment(int appointmentId)
        {

            var response = await _appointmentService.ConfirmAppointment(appointmentId);
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public async Task<JsonResult> UpdateAppointment(UpdateAppointmentViewModel appointment)
        {
            
                var response = await _appointmentService.UpdateAppointment(appointment.Id, appointment.StartDate, appointment.EndDate, appointment.PatientId, appointment.UserId);
                return Json(response, JsonRequestBehavior.AllowGet);
            
        }

        public async Task<JsonResult> setAppointment(CreateAppointmentViewModel appointment)
        {
            var response = await _appointmentService.CreateAppointment(appointment.StartDate, appointment.EndDate, appointment.PatientId, appointment.UserId);
            if (response.ExcecutedSuccesfully)
            {
                appointment.Id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
            }

            return Json(appointment,JsonRequestBehavior.AllowGet);
        }
    }
}