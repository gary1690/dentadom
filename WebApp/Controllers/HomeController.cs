﻿using System.Web.Mvc;
using AppService.Services;
using Domain.Global.Enums;
using Domain.Global.Extensions;
using WebApp.Helpers;
using WebApp.ViewModels;
using System.Globalization;
using System;
using System.Threading.Tasks;

namespace WebApp.Controllers
{
    [AuthenticationRequired]
    public class HomeController : BaseController
    {
        private readonly IAppointmentService _appointmentService;
        private readonly IInvoiceService _invoiceService;

        public HomeController( IAppointmentService appointmentService,IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
            _appointmentService = appointmentService;
        }

        // GET: Home
        public async Task<ActionResult> Dashboard()
        {
            var dashboardModel = new DashboardViewModel();
            var today = DateTime.Now;
            var firstInterval = today.Year + "-" + (today.Month > 9 ? today.Month.ToString() : "0" + today.Month) + '-' + (today.Day > 9 ? today.Day.ToString() : "0" + today.Day) + "T00:00:00";
            var lastInterval = today.Year + "-" + (today.Month > 9? today.Month.ToString():"0"+ today.Month) + '-' + (today.Day > 9 ? today.Day.ToString() : "0" + today.Day) + "T23:59:59";
            var todayAppointmentresult = await _appointmentService.GetAppointmentsByDateInterVal(DateTime.ParseExact(firstInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture), DateTime.ParseExact(lastInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture));
            if (todayAppointmentresult.ExcecutedSuccesfully)
            {
                if(todayAppointmentresult.Data != null)
                {
                    dashboardModel.TodayAppointment = todayAppointmentresult.Data;
                    dashboardModel.TodayAppointmentsCount = todayAppointmentresult.Data.Count;
                }else
                {
                    dashboardModel.TodayAppointmentsCount = 0;
                }
            }
            var todayIncomeResult = await _invoiceService.GetInvoiceByDateInterval(DateTime.ParseExact(firstInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture), DateTime.ParseExact(lastInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture));
            if (todayIncomeResult.ExcecutedSuccesfully)
            {
                if (todayIncomeResult.Data != null)
                {
                    decimal todayIncome = 0;
                    foreach(var invoice in todayIncomeResult.Data)
                    {
                        foreach (var invoiceDetails in invoice.InvoiceDetails)
                        {
                            todayIncome += invoiceDetails.MedicalProcedurePrice;
                        }
                    }
                    dashboardModel.TodayIncome = todayIncome;
                }
                else
                {
                    dashboardModel.TodayIncome = 0;
                }
            }
            firstInterval = today.Year + "-" + (today.Month > 9 ? today.Month.ToString() : "0" + today.Month) + '-' + "01T00:00:00";
            lastInterval = today.Year + "-" + ((today.Month+1) > 9 ? (today.Month+1).ToString() : "0" + (today.Month+1)) + '-' + "01T00:00:00";
            var thisMonthAppointmentResult = await _appointmentService.GetAppointmentsByDateInterVal(DateTime.ParseExact(firstInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture), DateTime.ParseExact(lastInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture));
            if (thisMonthAppointmentResult.ExcecutedSuccesfully)
            {
                if (thisMonthAppointmentResult.Data != null)
                {
                    dashboardModel.ThisMonthAppointmentsCount = thisMonthAppointmentResult.Data.Count;
                }else
                {
                    dashboardModel.ThisMonthAppointmentsCount = 0;
                }
            }
            var thisMonthIncomeResult = await _invoiceService.GetInvoiceByDateInterval(DateTime.ParseExact(firstInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture), DateTime.ParseExact(lastInterval, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture));
            if (thisMonthIncomeResult.ExcecutedSuccesfully)
            {
                if (thisMonthIncomeResult.Data != null)
                {
                    decimal thisMonthIncome = 0;
                    foreach (var invoice in thisMonthIncomeResult.Data)
                    {
                        foreach (var invoiceDetails in invoice.InvoiceDetails)
                        {
                            thisMonthIncome += invoiceDetails.MedicalProcedurePrice;
                        }
                    }
                    dashboardModel.ThisMonthIncome = thisMonthIncome;
                }
                else
                {
                    dashboardModel.ThisMonthIncome = 0;
                }
            }
            return View(dashboardModel);
        }

        public JsonResult ToDoListAdd(string reminder)
        {
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ToDoListDelete(int id)
        {
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ErrorPage(ErrorViewModel viewModel)
        {
            //var todayAppointments = _appointmentService
            return View(viewModel);
        }
    }
}