﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AppService.Framework;
using AppService.Services;
using Domain.Global.Dto;
using Microsoft.Ajax.Utilities;
using WebApp.Helpers;
using WebApp.ViewModels.InvoiceReport;

namespace WebApp.Controllers
{
    [AuthenticationRequired]
    public class InvoiceReportDetailedController : BaseController
    {
        private readonly IInvoiceService _invoiceService;

        public InvoiceReportDetailedController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        // GET: InvoiceReport
        [AuthenticationRequired]
        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        [AuthenticationRequired]
        public async Task<ActionResult> PrintReport(InvoiceReportDto invoiceReportDto)
        {
            if (!ModelState.IsValid)
                RedirectToHomeWithError(new ResponseModel
                {
                    Messages = new List<string> {"Ocurrio un Error con la Fechas Insertadas"}
                });

            var result = await _invoiceService.GetByDatesInterval(invoiceReportDto);

            if (!result.ExcecutedSuccesfully)
                RedirectToHomeWithError(result);

            var report = _invoiceService.GetDetailedInvoiceReport(result.Data);

            return FileAsStreamResult(report.Result.FileContent);
        }

        [HttpGet]
        [AuthenticationRequired]
        public async Task<ActionResult> Invoices(InvoiceReportDto invoiceReportDto)
        {
            if (invoiceReportDto.FromDate.IsNullOrWhiteSpace() || invoiceReportDto.ToDate.IsNullOrWhiteSpace())
            {
                RedirectToHomeWithError(new ResponseModel
                {
                    Messages = new List<string> {"Ocurrio un Error con la Fechas Insertadas"}
                });
            }
            var result = await _invoiceService.GetByDatesInterval(invoiceReportDto);

            if (!result.ExcecutedSuccesfully)
                RedirectToHomeWithError(result);

            var viewModel = new InvoiceReportViewModel
            {
                FromDate = result.Data.FirstOrDefault()?.FromDate,
                ToDate = result.Data.FirstOrDefault()?.ToDate,
                InvoiceReportDto = result.Data
            };

            return PartialView("~/Views/InvoiceReportDetailed/_list.cshtml", viewModel);
        }
    }
}