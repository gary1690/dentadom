﻿using AppService.Services;
using Data.Entities;
using Domain.Global;
using Domain.Global.Convertions;
using Domain.Global.Enums;
using Domain.Global.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.ViewModels;
using WebApp.ViewModels.Maintenance;
using WebApp.ViewModels.User;

namespace WebApp.Controllers
{
    [AuthenticationRequired]
    [Administrator]
    [CheckAccess]
    public class MaintenanceController : BaseController
    {

        private readonly IMedicalProcedureService _medicalProcedureService;
        private readonly IUserService _userService;
        public MaintenanceController(IMedicalProcedureService medicalProcedureService, IUserService userService)
        {
            _medicalProcedureService = medicalProcedureService;
            _userService = userService;
        }

        // Medical Procedure Maintenance
        public ActionResult MedicalProcedureMaintenance()
        {

            return View();

        }

        public async Task<JsonResult> CreateProcedure(CreateProcedureViewModel procedure)
        {
            var response = await _medicalProcedureService.CreateAsync(procedure.Description, procedure.Price);
            if (response.ExcecutedSuccesfully)
            {
                return Json(response.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> MedicalProceduresFeed()
        {
            var response = await _medicalProcedureService.GetAllAsync();
            if (response.ExcecutedSuccesfully)
            {
                return Json(response.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }

        public async Task<JsonResult> UpdateProcedure(UpdateProcedureViewModel procedure)
        {
            var response = await _medicalProcedureService.UpdateAsync(procedure.Id, procedure.Description, procedure.Price);
            if (response.ExcecutedSuccesfully)
            {
                return Json(response.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> DeleteProcedure(int id)
        {
            var response = await _medicalProcedureService.DeleteAsync(id);
            if (response.ExcecutedSuccesfully)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        //User Maintenance
        public async Task<ActionResult> UserMaintenance()
        {
            var users = await _userService.GetAllAsync();

            var viewModel = new UsersViewModel();

            if (users.ExcecutedSuccesfully)
            {
                viewModel.Users = users.Data;
            }
            return View(viewModel);
        }

        public async Task<ActionResult> New()
        {
            var viewModel = new ModalGenericViewModel
            {
                Icon = "fa fa-user",
                Title = "Nuevo Usuario",
                FormAction = Url.Action("New", "Maintenance"),
                FormControls = "~/Views/Maintenance/Views/_New.cshtml",
                UseAntiForgeryToken = false,
                FormId = "frm-new-user",
                FormMethod = FormMethod.Post
            };

            var roles = await _userService.GetAllRoles();

            if (roles.ExcecutedSuccesfully)
            {
                viewModel.FormViewModel = new NewUserViewModel
                {
                    RoleType = new SelectList(roles.Data, "Id",
                    "Description")
                };
            }

            return PartialView("~/Views/Maintenance/_ModalGeneric.cshtml", viewModel);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> New(NewUserViewModel model)
        {
            if (!ModelState.IsValid) return Json(new { }, JsonRequestBehavior.AllowGet);

            var user = new User
            {
                Name = model.Name,
                LastName = model.LastName,
                UserName = model.UserName,
                Password = model.Password
            };

            var role = new List<Role>();
            if (model.SelectedRoleType.Contains(RoleTypeEnum.Administrator))
                role.Add(new Role() { Id = (int)RoleTypeEnum.Administrator });
            if (model.SelectedRoleType.Contains(RoleTypeEnum.Dentist))
                role.Add(new Role() { Id = (int)RoleTypeEnum.Dentist });
            if (model.SelectedRoleType.Contains(RoleTypeEnum.Secretary))
                role.Add(new Role() { Id = (int)RoleTypeEnum.Secretary });
            if (model.SelectedRoleType.Contains(RoleTypeEnum.DentalAssistant))
                role.Add(new Role() { Id = (int)RoleTypeEnum.DentalAssistant });
            user.Roles = role;


            if (model.Photo != null)
            {
                user.ImageName = user.Name + Path.GetExtension(model.Photo.FileName);
                model.Photo.SaveAs(Server.MapPath("~/Images/" + user.ImageName));
                var image = FilesConvertion.ConvertToBytes(model.Photo);
                user.Image = image.File;
                user.Photo = ("/Images/" + user.ImageName);

            }

            var result = await _userService.CreateAsync(user);
            if (result.ExcecutedSuccesfully)
            {
                return Json("Registrado", JsonRequestBehavior.AllowGet);
            }else
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> Edit(int id)
        {
            var result = await _userService.GetUserById(id);

            if (!result.ExcecutedSuccesfully)
            {
                return ModalWithError(result);
            }
            if (result.Data.Roles.Count() == 0) {
                return ModalWithError(result);
            }

            string [] selectedRoles = new string[result.Data.Roles.Count];
            for(int i = 0; i< result.Data.Roles.Count;i++ )
            {
                selectedRoles[i] = result.Data.Roles.ElementAt(i).Id.ToString();
            }

            
            var viewModel = new ModalGenericViewModel
            {
                Icon = "fa fa-user",
                Title = "Editando Usuario",
                FormAction = Url.Action("Edit", "Maintenance"),
                FormControls = "~/Views/Maintenance/Views/_Edit.cshtml",
                UseAntiForgeryToken = false,
                FormId = "frm-edit-user",
                FormMethod = FormMethod.Post,
                FormViewModel = new EditUserViewModel
                {
                    Id = result.Data.Id,
                    Name = result.Data.Name,
                    LastName = result.Data.LastName,
                    UserName = result.Data.UserName,
                    Password = result.Data.Password,
                    ConfirmPassword = result.Data.Password,
                    PhotoFromDb = result.Data.Image,
                    RoleType = new MultiSelectList(RoleTypeEnum.DentalAssistant.GetAllDiplayNames(), "Id", "DisplayName", selectedRoles)
                }
            };

            return PartialView("~/Views/Maintenance/_ModalGeneric.cshtml", viewModel);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> Edit(EditUserViewModel model)
        {
            if (!ModelState.IsValid) return Json(new { }, JsonRequestBehavior.AllowGet);

            var user = new User
            {
                Id = model.Id,
                Name = model.Name,
                LastName = model.LastName,
                UserName = model.UserName,
                Password = model.Password
            };

           
            var role = new List<Role>();
            if (model.SelectedRoleType.Contains(RoleTypeEnum.Administrator))
                role.Add(new Role() { Id = (int)RoleTypeEnum.Administrator });
            if (model.SelectedRoleType.Contains(RoleTypeEnum.Dentist))
                role.Add(new Role() { Id = (int)RoleTypeEnum.Dentist });
            if (model.SelectedRoleType.Contains(RoleTypeEnum.Secretary))
                role.Add(new Role() { Id = (int)RoleTypeEnum.Secretary });
            if (model.SelectedRoleType.Contains(RoleTypeEnum.DentalAssistant))
                role.Add(new Role() { Id = (int)RoleTypeEnum.DentalAssistant });
            user.Roles = role;


            if (model.Photo != null)
            {
                user.ImageName = user.Name + Path.GetExtension(model.Photo.FileName);
                model.Photo.SaveAs(Server.MapPath("~/Images/"+user.ImageName));
                var image = FilesConvertion.ConvertToBytes(model.Photo);
                user.Image = image.File;
                user.Photo = ("/Images/" + user.ImageName);

            }

            var result = await _userService.UpdateAsync(user);
            if (result.ExcecutedSuccesfully)
            {
                return Json("Actualizado", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> Delete(int id)
        {
            var result = await _userService.Delete(id);
            if (result.ExcecutedSuccesfully)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}