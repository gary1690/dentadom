﻿using System.Linq;
using System.Web.Mvc;
using AppService.Framework;

namespace WebApp.Helpers
{
    public class ModelStateConverter
    {
        public static ResponseModel GetServiceResult(ModelStateDictionary modelStates)
        {
            var result = new ResponseModel();

            foreach (var modelStateError in modelStates.Values.Where(x => x.Errors.Count > 0).Select(x => x.Errors))
            {
                foreach (var error in modelStateError)
                {
                    result.AddErrorMessage(error.ErrorMessage);
                }
            }

            return result;
        }
    }
}