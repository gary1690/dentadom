﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Domain.Global.Enums;
using Domain.Global.Extensions;

namespace WebApp.Helpers
{
    public class SessionHelper
    {
        
        public static bool ExistUserInSession()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }
        public static void DestroyUserSession()
        {
            FormsAuthentication.SignOut();
            RemoveCurrentUserInfo();

        }
        public static int GetUser()
        {
            int user_id = 0;
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity is FormsIdentity)
            {
                FormsAuthenticationTicket ticket = ((FormsIdentity)HttpContext.Current.User.Identity).Ticket;
                if (ticket != null)
                {
                    user_id = Convert.ToInt32(ticket.UserData);
                }
            }
            return user_id;
        }
        public static void AddUserToSession(string id)
        {
            bool persist = true;
            var cookie = FormsAuthentication.GetAuthCookie("usuario", persist);

            cookie.Name = FormsAuthentication.FormsCookieName;
            cookie.Expires = DateTime.Now.AddMonths(3);

            var ticket = FormsAuthentication.Decrypt(cookie.Value);
            var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration, ticket.IsPersistent, id);

            cookie.Value = FormsAuthentication.Encrypt(newTicket);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void setUserInfo(ResponseModel response)
        {
            var user = response.Result;
            var roles = user.GetType().GetProperty("Roles").GetValue(response.Result, null);
            

            string name = user.GetType().GetProperty("Name").GetValue(response.Result, null);
            string lastname = user.GetType().GetProperty("LastName").GetValue(response.Result, null);
            string userRoles = "";
            string photo = user.GetType().GetProperty("Photo").GetValue(response.Result, null);
            string preferences = user.GetType().GetProperty("Preferences").GetValue(response.Result, null);
            foreach (var rol in roles)
            {
                userRoles += rol.GetType().GetProperty("Description").GetValue(rol, null) + ",";
            }
            HttpContext.Current.Session["UserName"] = name + " " + lastname;
            HttpContext.Current.Session["UserPhoto"] = photo;
            HttpContext.Current.Session["UserRoles"] = userRoles;
            setUserPreferences(preferences);
        }
        public static void setUserPreferences(string preferences)
        {
            HttpContext.Current.Session["UserPreferences"] = preferences;
        }

        public static void RemoveCurrentUserInfo( )
        {
           if(HttpContext.Current.Session.Count > 0)
            {
                HttpContext.Current.Session.Clear();
            }
        }
    }
    public class AuthenticationRequiredAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            HttpContext ctx = HttpContext.Current;
            if (!SessionHelper.ExistUserInSession())
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login"
                }));
            }
            else if (ctx.Request.IsAuthenticated)
            {


                if (ctx.Session != null)
                {

                    // check if a new session id was generated
                    if (ctx.Session.IsNewSession)
                    {

                        // If it says it is a new session, but an existing cookie exists, then it must
                        // have timed out
                      
                        if (ctx.Session.Count == 0)
                        {
                            FormsAuthentication.SignOut();
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                            {
                                controller = "Account",
                                action = "Login"
                            }));


                        }
                    }
                }

            }
            else
            {

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login"
                }));
            }
        }
    }

    // Si estamos logeado ya no podemos acceder a la página de Login
    public class AuthenticationNoRequiredAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (SessionHelper.ExistUserInSession())
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Dashboard"
                }));
            }
        }
    }

    public class ReferrOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (HttpContext.Current.Request.UrlReferrer == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Dashboard"
                }));
            }
        }
    }

    public class AdministratorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var roles = (HttpContext.Current.Session["UserRoles"]).ToString();
            if (roles.Contains("Administrador"))
            {
                if (HttpContext.Current.Session["Access"] == null || !((bool)HttpContext.Current.Session["Access"]))
                {
                    HttpContext.Current.Session["Access"] = true;
                }
            }
        }
    }

    public class SecretaryAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var roles = (HttpContext.Current.Session["UserRoles"]).ToString();
            if (roles.Contains("Secretaria"))
            {
                if (HttpContext.Current.Session["Access"] == null || !((bool)HttpContext.Current.Session["Access"]))
                {
                    HttpContext.Current.Session["Access"] = true;
                }
            }
        }
    }

    public class DentistAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var roles = (HttpContext.Current.Session["UserRoles"]).ToString();
            if (roles.Contains("Odontologo"))
            {
                if (HttpContext.Current.Session["Access"] == null || !((bool)HttpContext.Current.Session["Access"]))
                {
                    HttpContext.Current.Session["Access"] = true;
                }
            }
        }
    }

    public class DentalAssistantAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var roles = (HttpContext.Current.Session["UserRoles"]).ToString();
            if (roles.Contains("Asistente Dental"))
            {
                if (HttpContext.Current.Session["Access"] == null || !((bool)HttpContext.Current.Session["Access"]))
                {
                    HttpContext.Current.Session["Access"] = true;
                }
            }
        }
    }

    public class CheckAccessAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (HttpContext.Current.Session["Access"] != null)
            {
                if (((bool)HttpContext.Current.Session["Access"]))
                {
                    HttpContext.Current.Session["Access"] = false;
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Home",
                        action = "Dashboard"
                    }));
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Dashboard"
                }));
            }
        }
    }
}