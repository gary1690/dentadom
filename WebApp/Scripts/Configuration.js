﻿$('document').ready(function () {
    $('.save-preferences').click(function () {
        let preferences = { preferences: $('body').attr("class").replace('pace-done', '') }
        let jsondata = JSON.stringify(preferences);
        $.ajax({
            url: '@Url.Action("SetUserPreferences","Account")',
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            type: 'POST',
            data: jsondata,
            success: function (data) {
                swal("Configuracion Guardada", "", "success");
            }
        });
    });
});