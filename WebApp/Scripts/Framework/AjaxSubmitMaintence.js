﻿$(document).ready(function () {

    $("#btnDismissModal")
               .click(function () {
                   location.reload();
               });

    $("button[data-modal-ajax-submit='true']").click(function () {

        var button = $(this);
        var buttonForm = button.closest('form');
        var formMethod = buttonForm.attr('method');
        var formAction = buttonForm.attr('action');
        var target = ".modal-dialog";

        $.validator.unobtrusive.parse(buttonForm);

        buttonForm.submit(function (e) {
            e.preventDefault();

            if (buttonForm.valid()) {
                $.ajax({
                    type: formMethod,
                    url: formAction,
                    data: buttonForm.serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<img src="~/Scipts/Framework/Gifts/Spinner.gif" />',
                            css: {
                                border: 'none',
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function (data) {
                        alert("" + data);
                        $.unblockUI();

                    },
                    error: function (xhr) {
                        $.unblockUI();
                        console.log(xhr.statusText);
                    }
                });
            }
        
        });
    });
});