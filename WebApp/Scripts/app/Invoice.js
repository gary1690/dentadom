﻿$(document).on("click", ".createInvoice", function (event) {
    debugger;
    var button = $(this);
    var buttonForm = button.closest('form');
    var formMethod = buttonForm.attr('method');
    var formAction = buttonForm.attr('action');

    $.validator.unobtrusive.parse(buttonForm);

    buttonForm.submit(function (e) {
        debugger;
        e.preventDefault();
        if (buttonForm.valid()) {

            var formData = new FormData(this);
            //var token = $('[name=__RequestVerificationToken]').val();

            //formData.append("__RequestVerificationToken",token);

            $.ajax({
                type: formMethod,
                url: formAction,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                },
                success: function (data) {
                    debugger;
                    getResponseModelMessage(data);
                },
                error: function (xhr) {
                    console.log(xhr.statusText);
                }
            });
        }

    });
});

function getResponseModelMessage(data) {
    debugger;

    if (!data.hasOwnProperty("ExcecutedSuccesfully"))
        return false;

    if (data.ExcecutedSuccesfully.toString() === "true" || data.ExcecutedSuccesfully.toString() === "True") {

        var url = $("#RedirectTo").val() + "/" + data.Data;
        location.href = url;
    } else {

        var message = "";

        for (var i = 0; i < data.Messages.length ; i++) {
            message += data.Messages[i];
        }

        swal({
            title: 'Error!',
            text: message,
            type: "error"
        });
    }
    return false;
}