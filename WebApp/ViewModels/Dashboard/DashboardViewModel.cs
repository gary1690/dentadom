﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class DashboardViewModel
    {
        public int TodayAppointmentsCount { get; set; }
        public int ThisMonthAppointmentsCount { get; set; }
        public decimal TodayIncome { get; set; }
        public decimal ThisMonthIncome { get; set; }
        public List<Appointment> TodayAppointment { get; set;}
    }
}