﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class InventoryTransactionViewModel
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int Type { get; set; }

    }
}