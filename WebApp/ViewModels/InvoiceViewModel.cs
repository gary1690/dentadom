﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data.Entities;

namespace WebApp.ViewModels
{
    public class InvoiceViewModel
    {
        public Appointment Appointment { get; set; }
        public int AppointmentId { get; set; }
        public string DentistName { get; set; }
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
        public SelectList MedicalProcedures { get; set; }
        public Branch Branch { get; set; }
        public int[] procedureList { get; set; }

        [Display(Name = "Descuento")]
        public decimal Discount { get; set; }

        [Display(Name = "Seleccionar Procedimiento")]
        public int ProcedureSelected { get; set; }
        //[Required(ErrorMessage = "No ha Seleccionado Ningun Elemento")]
        //public decimal[] discountList { get; set; }
        //[Required(ErrorMessage = "No ha Seleccionado Ningun Elemento")]
        public decimal[] priceList { get; set; }
        [Required(ErrorMessage = "No ha Seleccionado Ningun Elemento")]
        public decimal[] taxAmountList { get; set; }
        [Display(Name = "Seleccionar Procedimiento")]
        public SelectList PaymentMethods { get; set; }
        [Display(Name = "Seleccionar Metodo de Pago")]
        public int PaymentMethodSelected { get; set; }
        [Display(Name = "   ")]
        public string Btn { get; set; }
    }
}