﻿using System.Web.Mvc;

namespace WebApp.ViewModels.Maintenance
{
    public class ModalGenericViewModel
    {
        public string Icon { get; set; }
        public bool IsAForm { get; set; } = true;
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string FormAction { get; set; }
        public string FormId { get; set; }
        public string FormControls { get; set; }
        public object FormViewModel { get; set; }
        public FormMethod FormMethod { get; set; } = FormMethod.Post;
        public bool UseAntiForgeryToken { get; set; } = false;
        public MvcHtmlString DiplayData { get; set; }
    }
}