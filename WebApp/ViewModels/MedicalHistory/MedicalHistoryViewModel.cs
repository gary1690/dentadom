﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class MedicalHistoryViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Indice de Periodontitis")]
        public int PDI { get; set; }
        [Display(Name = "Indice Gingival")]
        public int GI { get; set; }
        [Display(Name = "Indice de Placa")]
        public int PlI { get; set; }
        [Display(Name = "Indice de Higiene")]
        public int SHI { get; set; }
        public int PatientId { get; set; }
        public UpdatePatientViewModel Patient { get; set; }
        public List<TreatmentViewModel> Treatments { get; set; }
        public List<ConditionViewModel> Condictions { get; set; }
        public List<MedicationViewModel> Medications { get; set; }

        public static MedicalHistoryViewModel getMedical()
        {
            var Medical = new MedicalHistoryViewModel();
            Medical.Patient = new UpdatePatientViewModel();
            Medical.Treatments = new List<TreatmentViewModel>();
            Medical.Condictions = new List<ConditionViewModel>();
            Medical.Medications = new List<MedicationViewModel>();
            return Medical;
        }
    }
}