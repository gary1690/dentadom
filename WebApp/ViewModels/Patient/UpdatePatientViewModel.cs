﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class UpdatePatientViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Fecha de Nacimiento")]
        public string Birthday { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneType { get; set; }
    }
}