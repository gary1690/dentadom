﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class CreateProductViewModel
    {
        public string Name { get; set; }
        public string Brand { get; set; }
        public int SupplierId { get; set; }
        public double UnitCost { get; set; }
        public int Quantity { get; set; }
    }
}