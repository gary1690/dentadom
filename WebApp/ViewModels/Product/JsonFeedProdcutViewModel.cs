﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.ViewModels;

namespace WebApp.ViewModels
{
    public class JsonFeedProdcutViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public int SupplierId { get; set; }
        public string Supplier { get; set; }
        public double UnitCost { get; set; }
        public int Quantity { get; set; }

        public static List<JsonFeedProdcutViewModel> Products()
        {
            return new List<JsonFeedProdcutViewModel>()
            {
                new JsonFeedProdcutViewModel()
                {
                    Id = 1,
                    Name = "Bisturi",
                    Brand = "Material",
                    Supplier = JsonFeedSupplierViewModel.SupplierList().ElementAt(0).Name,
                    SupplierId = JsonFeedSupplierViewModel.SupplierList().ElementAt(0).Id,
                    UnitCost = 23.5,
                    Quantity =100
                },
                new JsonFeedProdcutViewModel()
                {
                    Id = 2,
                    Name = "Sellante",
                    Brand = "Material",
                    Supplier = JsonFeedSupplierViewModel.SupplierList().ElementAt(0).Name,
                    SupplierId = JsonFeedSupplierViewModel.SupplierList().ElementAt(0).Id,
                    UnitCost = 12.5,
                    Quantity =60
                },
                new JsonFeedProdcutViewModel()
                {
                    Id = 3,
                    Name = "Clean Paste",
                    Brand = "Material",
                    Supplier = JsonFeedSupplierViewModel.SupplierList().ElementAt(1).Name,
                    SupplierId = JsonFeedSupplierViewModel.SupplierList().ElementAt(1).Id,
                    UnitCost = 10.5,
                    Quantity =80
                },
                new JsonFeedProdcutViewModel()
                {
                    Id = 4,
                    Name = "Babero Desechable",
                    Brand = "Material",
                    Supplier = JsonFeedSupplierViewModel.SupplierList().ElementAt(2).Name,
                    SupplierId = JsonFeedSupplierViewModel.SupplierList().ElementAt(2).Id,
                    UnitCost = 5.5,
                    Quantity =120
                }
            };
        }
    }
}