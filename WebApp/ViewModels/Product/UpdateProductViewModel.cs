﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class UpdateProductViewModel
    {
        public int Id { get; set; }
        [Display(Name="Articulo")]
        public string Name { get; set; }
        [Display(Name = "Marca")]
        public string Brand { get; set; }
        [Display(Name = "Suplidor")]
        public int SupplierId { get; set; }
        [Display(Name = "Costo Unitario")]
        public double UnitCost { get; set; }
        [Display(Name = "Cantidad")]
        public int Quantity { get; set; }
    }
}