﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class JsonFeedSupplierViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public static List<JsonFeedSupplierViewModel> SupplierList()
        {
            return new List<JsonFeedSupplierViewModel>()
            {
                new JsonFeedSupplierViewModel()
                {
                    Id =1,
                    Name = "Dentist CxA",
                    Address = "C/ Roberto Pasteuriza # 8",
                    Phone = "809-334-8365"
                },
                new JsonFeedSupplierViewModel()
                {
                    Id =2,
                    Name = "Teeth CxA",
                    Address = "Ave Castellanos # 1",
                    Phone = "809-454-5656"
                },
                new JsonFeedSupplierViewModel()
                {
                    Id =3,
                    Name = "Braquets CxA",
                    Address = "C/ Roberto Pasteuriza # 8",
                    Phone = "809-334-8365"
                },
            };
        }
    }

}