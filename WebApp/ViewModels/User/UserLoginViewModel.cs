﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.ViewModels
{
    public class UserLoginViewModel
    {
        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "Un nombre de ususario es requerido")]
        public string UserName { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "Una contraseña es requerida")]
        public string Password { get; set; }
    }
}