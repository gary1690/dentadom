﻿using System.Collections.Generic;

namespace WebApp.ViewModels.User
{
    public class UsersViewModel
    {
        public List<Data.Entities.User> Users { get; set; }
    }
}